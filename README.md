# transfer-file

##### 同步本机指定目录下的文件到Kafka。*目前只支持一级目录*。将会把异常堆栈作为一条日志上传只Kafka
- [x] 监听文件夹变化
- [x] 监听文件变化
- [x] 发送到kafka


##### 依赖
- go get github.com/fsnotify/fsnotify
- go get github.com/json-iterator/go
- go get github.com/robfig/cron
- go get github.com/Shopify/sarama
- go get github.com/urfave/cli


##### 启动
```
./transfer-file --name test --dataDir /Users/huqichao/temp --path /Users/huqichao/temp/test --kafkaHost 192.168.16.103:9092 --kafkaTopic file_log2
```

#### 日志样例
```
2018-08-10 09:23:27:343|ERROR|ssdasdsada===33|3333--fffff
```

##### 发送到Kafka的消息模板
```
{"Application":"test","FileName":"/Users/huqichao/temp/test/eee.txt","Host":"192.168.30.48","Time":"2018-08-10 09:23:27:343","Level":"ERROR","Body":"ssdasdsada===33|3333--fffff"}
```

##### Mac下编译不同环境可执行文件
Windows
```
GOOS=windows GOARCH=amd64 go build -ldflags '-w -s' -o ./bin/transfer-file.exe ./src/main.go
```
Mac
```
go build -ldflags '-w -s' -o ./bin/transfer-file ./src/main.go
```
Linux
```
GOOS=linux GOARCH=amd64 go build -ldflags '-w -s' -o ./bin/transfer-file ./src/main.go
```

##### 压缩
```
brew install upx
upx transfer-file.exe
upx transfer-file
```