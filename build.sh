#!/bin/sh

version=`grep "const VERSION" ./src/main.go | awk '{print $4}'`
len=${#version}
version=${version:1:len - 2}

#mac下编译可执行文件
go build -ldflags '-w -s' -o ./bin/transfer-file ./src/main.go
cd bin
upx transfer-file
tar -czvf "transfer-file_${version}_darwin_amd64.tar.gz" transfer-file
rm -rf transfer-file
cd ..

#mac下编译windows可执行文件
GOOS=windows GOARCH=amd64 go build -ldflags '-w -s' -o ./bin/transfer-file.exe  ./src/main.go
cd bin
upx transfer-file.exe
tar -czvf "transfer-file_${version}_windows_amd64.tar.gz" transfer-file.exe
rm -rf transfer-file.exe
cd ..

#mac下编译linux可执行文件
GOOS=linux GOARCH=amd64 go build -ldflags '-w -s' -o ./bin/transfer-file  ./src/main.go
cd bin
upx transfer-file
tar -czvf "transfer-file_${version}_linux_amd64.tar.gz" transfer-file
rm -rf transfer-file
cd ..