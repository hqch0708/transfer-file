package Log

import (
	"../base"

	"fmt"
			)

func InfoWithFormat(format string, a... interface{})  {
	content := fmt.Sprintf(format, a...)
	log := base.NewLog(content, "INFO")
	fmt.Print(log)
}

func ErrorWithFormat(format string, a... interface{})  {
	content := fmt.Sprintf(format, a...)
	log := base.NewLog(content, "ERROR")
	fmt.Print(log)
}

func WarnWithFormat(format string, a... interface{})  {
	content := fmt.Sprintf(format, a...)
	log := base.NewLog(content, "WARN")
	fmt.Print(log)
}

func DebugWithFormat(format string, a...interface{})  {
	content := fmt.Sprintf(format, a...)
	log := base.NewLog(content, "DEBUG")
	fmt.Print(log)
}


func Info(a interface{})  {
	log := base.NewLog(a, "INFO")
	fmt.Print(log)
}

func Error(a interface{})  {
	log := base.NewLog(a, "ERROR")
	fmt.Print(log)
}

func Warn(a interface{})  {
	log := base.NewLog(a, "WARN")
	fmt.Print(log)
}

func Debug(a interface{})  {
	log := base.NewLog(a, "DEBUG")
	fmt.Print(log)
}
