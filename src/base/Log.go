package base

import (
	"../util"
	"time"
	"runtime"
	"fmt"
	"strings"
)

type Log struct {
	ClassName string
	MethodName string
	Level string
	Line int
	Content interface{}
	Time time.Time
	Pattern string
	Gid uint64
}

func NewLog(content interface{}, level string) Log {
	pc,file,line,_ := runtime.Caller(2)
	f := runtime.FuncForPC(pc)
	methodName := f.Name()
	if strings.Contains(methodName, "/") {
		methodName = methodName[strings.LastIndex(methodName, "/") + 1:]
	}
	gid := util.GetGID()
	return Log{ClassName: file, Level: level, Line: line, Content: content,
			Time: time.Now(), MethodName: methodName, Gid: gid, Pattern: "%s %5s %d %s:%d [%s] %s\n"}
}

func (log Log) String() string {
	return fmt.Sprintf(log.Pattern, util.FormatTime(log.Time), log.Level, log.Gid, log.ClassName, log.Line, log.MethodName, log.Content)
}