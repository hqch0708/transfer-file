package fileConfig

import (
	"../model"
	"../Log"
	"../util"

	"github.com/json-iterator/go"
	"io/ioutil"
	"sync"
	"os"
	"strings"
)

var once sync.Once
var this *FileConfigManager

type FileConfigManager struct {
	mu sync.Mutex
	Config model.Config
	FileConfigMap map[string]model.FileConfig
}

func GetInstance() *FileConfigManager{
	once.Do(func() {
		this = &FileConfigManager{}
	})
	return this
}

func SetFileConfigMap(json string) {
	fileConfigMap := make(map[string]model.FileConfig)
	var jsonIterator = jsoniter.ConfigCompatibleWithStandardLibrary
	jsonIterator.Unmarshal([]byte(json), &fileConfigMap)
	this.FileConfigMap = fileConfigMap
}

func GetFileConfigByFileName(fileName string) model.FileConfig {
	fileConfig, ok := this.FileConfigMap[fileName]
	if ok {
		return fileConfig
	} else {
		fileConfig = model.NewFileConfig(fileName)
		AddConfig(fileConfig)
		return fileConfig
	}
}

func AddConfig(config model.FileConfig)  {
	fileInfo, _ := os.Stat(config.FilePath)
	if isFile(fileInfo) {
		fileMap := this.FileConfigMap
		fileMap[config.FilePath] = config

		Write()
	}
}

func RemoveConfig(config model.FileConfig)  {
	fileMap := this.FileConfigMap
	delete(fileMap, config.FilePath)

	Write()
}

func ReplaceConfig(config model.FileConfig)  {
	fileMap := this.FileConfigMap
	delete(fileMap, config.FilePath)
	fileMap[config.FilePath] = config

	Write()
}

func Write() {
	this.mu.Lock()
	var jsonIterator = jsoniter.ConfigCompatibleWithStandardLibrary
	json, err := jsonIterator.MarshalToString(this.FileConfigMap)
	if err != nil {
		Log.ErrorWithFormat("配置文件转换JSON失败")
	}

	data := []byte(json)
	err = ioutil.WriteFile(this.Config.DataFilePath, data,0644)
	if err == nil {
		Log.DebugWithFormat("写入文件成功: %s", this.Config.DataFilePath)
	} else {
		Log.ErrorWithFormat("写入配置文件失败: %s", this.Config.DataFilePath)
		Log.Error(err)
	}

	this.mu.Unlock()
}

func WriteByBatch(fileIndexMap map[string]int) {
	for k, v := range fileIndexMap {
		if fileConfig, ok := this.FileConfigMap[k]; !ok {
			fileConfig := model.NewFileConfig(k)
			fileConfig.LastScanLine = v
			fileConfig.LastScanTime = util.NowTimeStr()

			this.FileConfigMap[k] = fileConfig
		} else {
			fileConfig.LastScanLine = v
			fileConfig.LastScanTime = util.NowTimeStr()

			this.FileConfigMap[k] = fileConfig
		}
	}

	Write()
}


func Print()  {
	Log.Debug(this.FileConfigMap)
}


func isFile(fileInfo os.FileInfo) bool {
	if fileInfo.IsDir() {
		Log.DebugWithFormat("当前仅支持一级文件夹: %s", fileInfo.Name())
		return false
	}
	if strings.HasPrefix(fileInfo.Name(), ".") {
		Log.DebugWithFormat("当前文件为隐藏文件: %s", fileInfo.Name())
		return false
	}
	return true
}