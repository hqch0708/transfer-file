package kafka

import "fmt"

type KafkaLog struct {
	Content string
	FilePath string
	Line int
}

func NewKafkaLog(content string, filePath string, line int) KafkaLog {
	return KafkaLog{Content: content, FilePath: filePath, Line: line}
}

func (log KafkaLog) String() string {
	return fmt.Sprintf("Content: %s, FilePath: %s, Line: %d", log.Content, log.FilePath, log.Line)
}