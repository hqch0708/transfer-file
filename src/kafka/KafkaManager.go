package kafka

import (
	"../Log"
	"../model"
	"../fileConfig"
	"../parser"
	"../util"
	"sync"
	"container/list"
	"strings"
	"github.com/json-iterator/go"
	"github.com/Shopify/sarama"
	"time"
	"log"
	"os"
)


var once sync.Once
var this *KafkaManager

type KafkaManager struct {
	mu sync.Mutex
	Host string
	LogList list.List
	Config model.Config
	Parser parser.ParserLog
	LastLog KafkaLog
	LastOk bool
	HavePattern bool
}

func GetInstance(config model.Config) *KafkaManager{
	once.Do(func() {
		this = &KafkaManager{}
		this.Config = config
		if len(config.Pattern) > 0 {
			this.Parser = parser.NewParserLog(config.Pattern)
			this.HavePattern = true
		} else {
			this.HavePattern = false
		}
		this.LastOk = true
	})
	return this
}

func AppendLog(log KafkaLog) {
	this.mu.Lock()
	if len(log.Content) > 0 {
		this.LogList.PushBack(log)
	}
	this.mu.Unlock()
}

func parserLogByPattern() ([]*sarama.ProducerMessage, map[string]int) {
	this.mu.Lock()
	if this.LogList.Len() == 0 {
		this.mu.Unlock()
		return nil, nil
	}

	fileIndexMap := make(map[string]int)
	tmpLogSlice := make([]KafkaLog, this.LogList.Len())
	var jsonIterator = jsoniter.ConfigCompatibleWithStandardLibrary
	i := 0
	splitLen := 0
	for e := this.LogList.Front(); e != nil; {
		log := e.Value.(KafkaLog)
		fileIndexMap[log.FilePath] = log.Line
		strs := this.Parser.Split(log.Content)
		if splitLen == 0 {
			splitLen = len(strs)
		}

		if len(strs) >= splitLen {
			if !this.LastOk {
				tmpLogSlice[i - 1] = this.LastLog
			}
			this.LastOk = true

			this.LastLog = log
			tmpLogSlice[i] = log
			i++
		} else {
			this.LastLog.Content += "\n" + log.Content
			this.LastLog.Line = log.Line
			this.LastOk = false
		}

		next := e.Next()
		this.LogList.Remove(e)
		e = next
	}

	if !this.LastOk {
		tmpLogSlice = tmpLogSlice[:i - 1]
	}

	if len(tmpLogSlice) == 0 {
		return nil, nil
	}

	tmpMsgSlice := make([]*sarama.ProducerMessage, i)

	i = 0
	for _, tmpLog := range tmpLogSlice {
		if len(tmpLog.Content) == 0 {
			continue
		}
		Log.DebugWithFormat("发送Kafka: %s", tmpLog.Content)
		strs := this.Parser.Split(tmpLog.Content)
		json, _ := jsonIterator.MarshalToString(model.NewFMLog(this.Config.Name, tmpLog.FilePath, this.Parser.ParserTime(strs),
			this.Parser.ParserLevel(strs), this.Parser.ParserBody(strs)))
		//创建消息
		msg := sarama.ProducerMessage{}
		msg.Topic = this.Config.KafkaTopic
		msg.Value = sarama.StringEncoder(json)
		tmpMsgSlice[i] = &msg
		i++
	}

	this.mu.Unlock()
	return tmpMsgSlice, fileIndexMap
}

func parserLogByNormal() ([]*sarama.ProducerMessage, map[string]int) {
	this.mu.Lock()
	if this.LogList.Len() == 0 {
		this.mu.Unlock()
		return nil, nil
	}
	i := 0
	fileIndexMap := make(map[string]int)
	tmpLogSlice := make([]KafkaLog, this.LogList.Len())
	for e := this.LogList.Front(); e != nil;  {
		log := e.Value.(KafkaLog)
		fileIndexMap[log.FilePath] = log.Line

		tmpLogSlice[i] = log
		i++

		next := e.Next()
		this.LogList.Remove(e)
		e = next
	}

	var jsonIterator = jsoniter.ConfigCompatibleWithStandardLibrary
	tmpMsgSlice := make([]*sarama.ProducerMessage, i)
	i = 0
	for _, tmpLog := range tmpLogSlice {
		if len(tmpLog.Content) == 0 {
			continue
		}
		Log.DebugWithFormat("发送Kafka: %s", tmpLog.Content)
		json, _ := jsonIterator.MarshalToString(model.NewFMLog(this.Config.Name, tmpLog.FilePath, util.NowTimeStr(),
			"INFO", tmpLog.Content))
		//创建消息
		msg := sarama.ProducerMessage{}
		msg.Topic = this.Config.KafkaTopic
		msg.Value = sarama.StringEncoder(json)
		tmpMsgSlice[i] = &msg
		i++
	}

	this.mu.Unlock()
	return tmpMsgSlice, fileIndexMap
}


func UploadToKafka() {

	var tmpMsgSlice []*sarama.ProducerMessage
	var fileIndexMap map[string]int

	if this.HavePattern {
		tmpMsgSlice, fileIndexMap = parserLogByPattern()
	} else {
		tmpMsgSlice, fileIndexMap = parserLogByNormal()
	}

	if tmpMsgSlice == nil {
		return
	}

	//设置sarama打印日志
	var logger = log.New(os.Stderr, "[TEST]", log.LstdFlags)
	sarama.Logger = logger

	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Partitioner = sarama.NewRandomPartitioner
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true
	config.Producer.Timeout = time.Second * 2
	//生产者
	producer, err := sarama.NewSyncProducer(strings.Split(this.Config.KafkaHost, ","), config)
	if err != nil {
		Log.ErrorWithFormat("Failed to produce message :%s", err)
		return
	}

	defer producer.Close()

	//发送消息
	err1 := producer.SendMessages(tmpMsgSlice)
	if err1 != nil {
		Log.Error(err1)
		return
	} else {
		fileConfig.WriteByBatch(fileIndexMap)
	}
}