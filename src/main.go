package main

import (
	"./model"
	"./Log"
	"./util"
	"./fileConfig"
	"./watch"
	"./monitor"
	"./kafka"
	"os"
	"github.com/fsnotify/fsnotify"
	"github.com/urfave/cli"
	"io/ioutil"
	"os/exec"
	"fmt"
	"os/signal"
)

const DEFAULT_SCAN_INTERVAL = 2

const DEFAULT_BATCHSIZE = 1000

const PID_FILE  = "/tmp/myid"

const VERSION = "1.0.4"

func main() {
	realMain()
}

func realMain() {
	config := new(model.Config)

	app := cli.NewApp()
	app.Version = VERSION
	app.Name = "transfer-file"
	app.Usage = "同步日志文件到Kafka"
	app.Flags = []cli.Flag {
		cli.StringFlag{
			Name: "name",
			Usage: "该名称将会作为上传到Kafka消息的Application字段",
			Destination: &config.Name,
		},
		cli.StringFlag{
			Name: "dataDir",
			Usage: "将存储监控的文件列表以及当前扫描到文件行数等配置信息",
			Destination: &config.DataDir,
		},
		cli.StringFlag{
			Name: "path",
			Usage: "监控的日志文件目录，目前只支持一级目录",
			Destination: &config.TransferPath,
		},
		cli.StringFlag{
			Name: "kafkaHost",
			Usage: "Kafka服务器地址，多个以,分割",
			Destination: &config.KafkaHost,
		},
		cli.StringFlag{
			Name: "kafkaTopic",
			Usage: "生成的Kafka消息主题",
			Destination: &config.KafkaTopic,
		},
		cli.IntFlag{
			Name: "batchSize",
			Value: DEFAULT_BATCHSIZE,
			Usage: "一次监控单个文件变化最大行数，要保证该值超过日志中记录异常堆栈的最大行",
			Destination: &config.BatchSize,
		},
		cli.IntFlag{
			Name: "interval",
			Value: DEFAULT_SCAN_INTERVAL,
			Usage: "指定间隔时间(秒)内，只会扫描到最大batchSize的数量日志行，并上传到Kafka",
			Destination: &config.ScanInterval,
		},
		cli.StringFlag{
			Name: "pattern",
			Usage: "日志文件格式，如: %d|%p|%m\n" +
					"\t\t%d 日期, 默认格式: 2006-01-02 15:04:05.000, 自定义格式 %d{2006/01/02}\n" +
					"\t\t%p 日志级别\n" +
					"\t\t%m 日志内容(必须在最后)\n\t\n" +
					"\t当该值为空，则将会把日志按行进行上传(日志格式比较凌乱时)",
			Destination: &config.Pattern,
		},
	}

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, os.Interrupt, os.Kill)
		for {
			s := <-c
			fmt.Println("Got signal:", s)
			stop()
			os.Exit(0)
		}
	}()

	app.Commands = []cli.Command{
		{
			Name:    "stop",
			Usage:   "停止应用",
			Action:  func(c *cli.Context) error {
				return stop()
			},
		},
	}

	app.Action = func(c *cli.Context) error {
		if len(c.String("name")) == 0 || len(c.String("dataDir")) == 0 || len(c.String("path")) == 0 || len(c.String("kafkaHost")) == 0 || len(c.String("kafkaTopic")) == 0 {
			cli.ShowAppHelp(c)
			return nil
		}
		start(config)
		return nil
	}

	app.Run(os.Args)
}

func stop() error {
	strb, _ := ioutil.ReadFile(PID_FILE)
	if len(string(strb)) == 0 {
		Log.InfoWithFormat("未启动应用")
		return nil
	}
	Log.InfoWithFormat("停止应用: %s", string(strb))
	exec.Command("kill", string(strb)).Start()
	os.Remove(PID_FILE)
	return nil
}

func start(oldConfig *model.Config) {
	pid, err1 := ioutil.ReadFile(PID_FILE)
	if err1 == nil {
		Log.InfoWithFormat("应用已启动: %s", pid)
		os.Exit(1)
	}


	config := model.CopyConfig(oldConfig)

	Log.DebugWithFormat("PID: %d", os.Getpid())
	Log.DebugWithFormat("IP: %s", util.GetLocalIp())
	Log.DebugWithFormat("Name: %s", config.Name)
	Log.DebugWithFormat("Kafka: %s", config.KafkaHost)
	Log.DebugWithFormat("Topic: %s", config.KafkaTopic)

	ioutil.WriteFile(PID_FILE, []byte(fmt.Sprintf("%d", os.Getpid())), 0666)

	_, err := os.Stat(config.DataFilePath)
	if err != nil {
		if os.IsNotExist(err) {
			Log.WarnWithFormat("%s 不存在，初始化配置", config.DataFilePath)
			os.MkdirAll(config.DataDir, os.ModePerm)
			_, err := os.Create(config.DataFilePath)
			if err != nil {
				Log.ErrorWithFormat("创建配置文件失败: %s", config.DataFilePath)
				os.Exit(1)
			}
		}
	}
	configFile, err := ioutil.ReadFile(config.DataFilePath)
	if err != nil {
		Log.ErrorWithFormat("读取配置文件失败: %s", config.DataFilePath)
		os.Exit(1)
	}

	body := string(configFile)
	Log.DebugWithFormat("配置文件内容: %s", body)


	Log.InfoWithFormat("检测文件夹 %s ...", config)

	dirList, e := ioutil.ReadDir(config.TransferPath)
	if e != nil {
		Log.ErrorWithFormat("获取检测文件列表失败: %s", config.TransferPath)
		os.Exit(1)
	}
	watchFileSlice := make([]string, 0)
	for _, v := range dirList {
		if util.IsFile(v) {
			Log.DebugWithFormat("检测文件: %s", v.Name())
			watchFileSlice = append(watchFileSlice, v.Name())
		}
	}

	kafkaManager := kafka.GetInstance(config)
	kafkaManager.Host = config.KafkaHost

	fileConfigManager := fileConfig.GetInstance()
	fileConfigManager.Config = config
	fileConfig.SetFileConfigMap(body)

	for _, value := range watchFileSlice {
		_config := fileConfig.GetFileConfigByFileName(config.TransferPath + "/" + value)

		appendFileWatcher := new(watch.AppendFileWatcher)
		appendFileWatcher.FilePath = config.TransferPath + "/" + value
		appendFileWatcher.Handler(_config, config.BatchSize)
	}

	watchFileChange := monitor.NewWatch()
	watchFileChange.Init(config)

	//监听文件夹
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		Log.Error("启动系统失败，无法创建监控")
		os.Exit(1)
	}

	defer watcher.Close()

	err = watcher.Add(config.TransferPath)
	if err != nil {
		Log.ErrorWithFormat("检测文件夹失败: %s", config.TransferPath)
		os.Exit(1)
	}

	go func() {
		for {
			select {
			case ev := <-watcher.Events:
				{

					_config := fileConfig.GetFileConfigByFileName(ev.Name)

					//判断事件发生的类型，如下5种
					// Create 创建
					// Write 写入
					// Remove 删除
					// Rename 重命名
					// Chmod 修改权限
					if ev.Op&fsnotify.Create == fsnotify.Create {
						fileInfo, _ := os.Stat(ev.Name)
						if util.IsFile(fileInfo) {
							createFileWatcher := new(watch.CreateFileWatcher)
							createFileWatcher.FilePath = ev.Name
							createFileWatcher.Handler(_config)
						}
					}
					//写入事件在AppendFileWatcher中监听
					if ev.Op&fsnotify.Write == fsnotify.Write {

					}
					if ev.Op&fsnotify.Remove == fsnotify.Remove {
						deleteFileWatcher := new(watch.DeleteFileWatcher)
						deleteFileWatcher.FilePath = ev.Name
						deleteFileWatcher.Handler(_config)
					}
					//Rename事件就是删除文件、真实Remove不会触发
					if ev.Op&fsnotify.Rename == fsnotify.Rename {
						deleteFileWatcher := new(watch.DeleteFileWatcher)
						deleteFileWatcher.FilePath = ev.Name
						deleteFileWatcher.Handler(_config)
					}
					//修改权限无需监听
					if ev.Op&fsnotify.Chmod == fsnotify.Chmod {
					}
				}
			case err := <-watcher.Errors:
				{
					Log.Error(err)
					return
				}
			}
		}
	}()

	//循环
	select {}
}