package main

import (
	"./model"
	"./Log"
	"./util"
	"./fileConfig"
	"./watch"
	"./monitor"
	"./kafka"
	"os"
	"github.com/fsnotify/fsnotify"
	"io/ioutil"
	"testing"
	"log"
	"github.com/Shopify/sarama"
)

const DATA_DIR  = "/Users/huqichao/temp/"
const PATH = "/Users/huqichao/temp/test/"
const PATTERN = "%d - %m"
const KAFKA_HOST  = "192.168.30.48:9092"
//const KAFKA_HOST  = "192.168.16.103:9092"
const KAFKA_TOPIC  = "file_log"

func Test_Main(t *testing.T) {
	//testKafka()

	var logger = log.New(os.Stderr, "[TEST]", log.LstdFlags)
	sarama.Logger = logger

	config := model.NewConfig("test", DATA_DIR, PATH, "", KAFKA_HOST, KAFKA_TOPIC)
	startTest(&config)
}

func startTest(oldConfig *model.Config) {
	config := model.CopyConfig(oldConfig)

	Log.DebugWithFormat("PID: %d", os.Getpid())
	Log.DebugWithFormat("IP: %s", util.GetLocalIp())
	Log.DebugWithFormat("Name: %s", config.Name)
	Log.DebugWithFormat("Kafka: %s", config.KafkaHost)
	Log.DebugWithFormat("Topic: %s", config.KafkaTopic)

	_, err := os.Stat(config.DataFilePath)
	if err != nil {
		if os.IsNotExist(err) {
			Log.WarnWithFormat("%s 不存在，初始化配置", config.DataFilePath)
			os.MkdirAll(DATA_DIR, os.ModePerm)
			_, err := os.Create(config.DataFilePath)
			if err != nil {
				Log.ErrorWithFormat("创建配置文件失败: %s", config.DataFilePath)
				os.Exit(1)
			}
		}
	}
	configFile, err := ioutil.ReadFile(config.DataFilePath)
	if err != nil {
		Log.ErrorWithFormat("读取配置文件失败: %s", config.DataFilePath)
		os.Exit(1)
	}

	body := string(configFile)
	Log.DebugWithFormat("配置文件内容: %s", body)


	Log.InfoWithFormat("检测文件夹 %s ...", config)

	dirList, e := ioutil.ReadDir(config.TransferPath)
	if e != nil {
		Log.ErrorWithFormat("获取检测文件列表失败: %s", config.TransferPath)
		os.Exit(1)
	}
	watchFileSlice := make([]string, 0)
	for _, v := range dirList {
		if util.IsFile(v) {
			Log.DebugWithFormat("检测文件: %s", v.Name())
			watchFileSlice = append(watchFileSlice, v.Name())
		}
	}

	kafkaManager := kafka.GetInstance(config)
	kafkaManager.Host = config.KafkaHost

	fileConfigManager := fileConfig.GetInstance()
	fileConfigManager.Config = config
	fileConfig.SetFileConfigMap(body)

	for _, value := range watchFileSlice {
		_config := fileConfig.GetFileConfigByFileName(config.TransferPath + "/" + value)

		appendFileWatcher := new(watch.AppendFileWatcher)
		appendFileWatcher.FilePath = config.TransferPath + "/" + value
		appendFileWatcher.Handler(_config, config.BatchSize)
	}

	watchFileChange := monitor.NewWatch()
	watchFileChange.Init(config)

	//监听文件夹
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		Log.Error("启动系统失败，无法创建监控")
		os.Exit(1)
	}

	defer watcher.Close()

	err = watcher.Add(config.TransferPath)
	if err != nil {
		Log.ErrorWithFormat("检测文件夹失败: %s", config.TransferPath)
		os.Exit(1)
	}

	go func() {
		for {
			select {
			case ev := <-watcher.Events:
				{

					_config := fileConfig.GetFileConfigByFileName(ev.Name)

					//判断事件发生的类型，如下5种
					// Create 创建
					// Write 写入
					// Remove 删除
					// Rename 重命名
					// Chmod 修改权限
					if ev.Op&fsnotify.Create == fsnotify.Create {
						fileInfo, _ := os.Stat(ev.Name)
						if util.IsFile(fileInfo) {
							createFileWatcher := new(watch.CreateFileWatcher)
							createFileWatcher.FilePath = ev.Name
							createFileWatcher.Handler(_config)
						}
					}
					//写入事件在AppendFileWatcher中监听
					if ev.Op&fsnotify.Write == fsnotify.Write {

					}
					if ev.Op&fsnotify.Remove == fsnotify.Remove {
						deleteFileWatcher := new(watch.DeleteFileWatcher)
						deleteFileWatcher.FilePath = ev.Name
						deleteFileWatcher.Handler(_config)
					}
					//Rename事件就是删除文件、真实Remove不会触发
					if ev.Op&fsnotify.Rename == fsnotify.Rename {
						deleteFileWatcher := new(watch.DeleteFileWatcher)
						deleteFileWatcher.FilePath = ev.Name
						deleteFileWatcher.Handler(_config)
					}
					//修改权限无需监听
					if ev.Op&fsnotify.Chmod == fsnotify.Chmod {
					}
				}
			case err := <-watcher.Errors:
				{
					Log.Error(err)
					return
				}
			}
		}
	}()

	//循环
	select {}
}