package model

import (
	"fmt"
	"strings"
)

const CONFIG_FILE  = "data.json"

type Config struct {
	Name string
	DataDir string
	DataFilePath string
	TransferPath string
	Pattern string
	KafkaHost string
	KafkaTopic string
	BatchSize int
	ScanInterval int	//扫描文件间隔秒值
}

func NewConfig(name string, dataDir string, path string, pattern string, kafkaHost string, kafkaTopic string) Config {
	have := strings.HasSuffix(path, "/")
	if have {
		path = path[:len(path) - 1]
	}
	have = strings.HasSuffix(dataDir, "/")
	if have {
		dataDir = dataDir[:len(dataDir) - 1]
	}
	return Config{DataDir: dataDir, TransferPath: path, Pattern: pattern,
			KafkaHost: kafkaHost, BatchSize: 100, ScanInterval: 2,
			DataFilePath: dataDir + "/" + CONFIG_FILE, KafkaTopic: kafkaTopic, Name: name}
}

func CopyConfig(config *Config) Config {
	have := strings.HasSuffix(config.TransferPath, "/")
	if have {
		config.TransferPath = config.TransferPath[:len(config.TransferPath) - 1]
	}
	have = strings.HasSuffix(config.DataDir, "/")
	if have {
		config.DataDir = config.DataDir[:len(config.DataDir) - 1]
	}

	return Config{DataDir: config.DataDir, TransferPath: config.TransferPath, Pattern: config.Pattern,
		KafkaHost: config.KafkaHost, BatchSize: config.BatchSize, ScanInterval: config.ScanInterval,
		DataFilePath: config.DataDir + "/" + CONFIG_FILE, KafkaTopic: config.KafkaTopic, Name: config.Name}
}

func (c Config) String() string {
	return fmt.Sprintf("DataDir: %s, TransferPath: %s, Pattern: %s, KafkaHost: %s, BatchSize: %d, ScanInterval: %d",
		c.DataDir, c.TransferPath, c.Pattern, c.KafkaHost, c.BatchSize, c.ScanInterval)
}