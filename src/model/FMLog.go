package model

import "../util"

type FMLog struct {
	Application string
	FileName string
	Host string
	Time string
	Level string
	Body string
}

func NewFMLog(appName string, fileName string, time string, level string, body string) FMLog {
	return FMLog{Application: appName, Time: time, Level: level, Body: body, Host: util.GetLocalIp(), FileName: fileName}
}