package model

import (
		"fmt"
		"../util"
)

type FileConfig struct {
	FilePath string
	LastScanTime string
	LastScanLine int
}

func NewFileConfig(filePath string) FileConfig {
	return FileConfig{FilePath: filePath, LastScanLine: 0, LastScanTime: util.NowTimeStr()}
}

func (c FileConfig) String() string {
	return fmt.Sprintf("FilePath: %s, LastScanTime: %s, LastScanLine: %d", c.FilePath, c.LastScanTime, c.LastScanLine)
}