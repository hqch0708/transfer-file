package monitor

import (
	"../kafka"
	"../Log"
	"../fileConfig"
	"../model"
	"os"
	"bufio"
	"github.com/robfig/cron"
	"fmt"
	"sync"
)

var once sync.Once

type WatchFileChange struct {
}


func NewWatch() WatchFileChange {
	return WatchFileChange{}
}

func (watchFileChange WatchFileChange) Init(config model.Config) {
	once.Do(func() {
		c := cron.New()
		c.AddFunc(fmt.Sprintf("@every %ds", config.ScanInterval), func() {
			watchFileChange.Watch(config)
		})
		c.Start()
	})
}

func (watchFileChange WatchFileChange) Watch(config model.Config)  {
	fileMap := fileConfig.GetInstance().FileConfigMap
	for _, value := range fileMap {
		file, _ := os.Open(value.FilePath)
		fileScanner := bufio.NewScanner(file)
		lineCount := 1
		appendCount := 0
		for fileScanner.Scan(){
			if lineCount > value.LastScanLine{
				if appendCount < config.BatchSize {
					kafka.AppendLog(kafka.NewKafkaLog(fileScanner.Text(), value.FilePath, lineCount))
				} else {
					break
				}
				appendCount++
			}
			lineCount++
		}

		//文件最后会多一行
		lineCount = lineCount - 1

		if lineCount > value.LastScanLine {
			Log.DebugWithFormat("同步文件:%s, %d->%d", value.FilePath, value.LastScanLine, lineCount)
		}
		defer file.Close()
	}

	kafka.UploadToKafka()
}
