package parser

import (
	"../Log"
	"../util"
	"fmt"
	"strings"
	"os"
)

const ESCAPE_CHAR = "%"
const SPLIT_CHAR  = "|"
const TIME_PATTERN  = "2006-01-02 15:04:05.000"

type ParserLog struct {
	Pattern string
	SplitStr string
	TimeIndex int
	LevelIndex int
	BodyIndex int
	TimePattern string
}

func NewParserLog(pattern string) ParserLog {
	var timeIndex = -1
	var levelIndex = -1
	var bodyIndex = -1
	var splitStr = "|"
	var orginTimeIndex = -1
	var timePattern string
	var index = 0
	for i , v := range pattern {
		s := fmt.Sprintf("%c", v)
		if s == ESCAPE_CHAR {
			continue
		}
		if s == "d" {
			timeIndex = index
			index ++
			orginTimeIndex = i
		} else if s == "p" {
			levelIndex = index
			index ++
		} else if s == "m" {
			bodyIndex = index
			index ++
		}
	}
	s := fmt.Sprintf("%c", pattern[len(pattern) - 1])
	if s != "m" {
		Log.ErrorWithFormat("%%m 必须在日志格式最后: %s", pattern)
		os.Exit(500)
	}

	if orginTimeIndex != -1 {
		timePattern = getTimePattern(pattern, orginTimeIndex)
		if strings.Index(pattern, "{") != -1 {
			splitStr = getSplitStr(pattern, strings.Index(pattern, "}") + 1)
		} else {
			splitStr = getSplitStr(pattern, 2)
		}
	} else {
		splitStr = getSplitStr(pattern, 2)
	}

	return ParserLog{Pattern: pattern, TimeIndex: timeIndex, LevelIndex: levelIndex, BodyIndex: bodyIndex, SplitStr: splitStr, TimePattern: timePattern}
}

func getSplitStr(pattern string, index int) string {
	for i , v := range pattern {
		s := fmt.Sprintf("%c", v)
		if i < index {
			continue
		}
		if s != " " {
			return s
		}
	}
	return SPLIT_CHAR
}

func getTimePattern(pattern string, index int) string {
	s := fmt.Sprintf("%c", pattern[index + 1])
	if s == "{" {
		endIndex := strings.Index(pattern, "}")
		return pattern[index + 2:endIndex]
	}
	return TIME_PATTERN
}

func (parser ParserLog) Split(content string) []string {
	return strings.Split(content, parser.SplitStr)
}

func (parser ParserLog) ParserTime(contents []string) string {
	if parser.TimeIndex == -1 {
		return util.NowTimeStr()
	}
	return util.FormatTimeByStr(strings.TrimSpace(contents[parser.TimeIndex]), parser.TimePattern)
}

func (parser ParserLog) ParserLevel(contents []string) string {
	if parser.LevelIndex == -1 {
		return "INFO"
	}
	return strings.TrimSpace(contents[parser.LevelIndex])
}

func (parser ParserLog) ParserBody(contents []string) string {
	return strings.TrimSpace(strings.Join(contents[parser.BodyIndex:], parser.SplitStr))
}