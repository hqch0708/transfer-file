package util

import (
	"runtime"
	"bytes"
	"strconv"
	"os"
	"strings"
	"fmt"
)

const PID_FILE  = "myid"

func GetGID() uint64 {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, false)]
	b = bytes.TrimPrefix(b, []byte("goroutine "))
	b = b[:bytes.IndexByte(b, ' ')]
	n, _ := strconv.ParseUint(string(b), 10, 64)
	return n
}


func IsFile(fileInfo os.FileInfo) bool {
	if fileInfo.IsDir() {
		fmt.Printf("当前仅支持一级文件夹: %s", fileInfo.Name())
		return false
	}
	if strings.HasPrefix(fileInfo.Name(), ".") {
		fmt.Printf("当前文件为隐藏文件: %s", fileInfo.Name())
		return false
	}
	if fileInfo.Name() == PID_FILE {
		return false
	}
	return true
}