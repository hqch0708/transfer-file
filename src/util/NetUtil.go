package util

import (
	"net"
	"fmt"
)

func GetLocalIp() string {
	addrSlice, err := net.InterfaceAddrs()
	if nil != err {
		fmt.Println("Get local IP addr failed!!!")
		return "127.0.0.1"
	}

	for _, addr := range addrSlice {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if nil != ipnet.IP.To4() {
				return ipnet.IP.String()
			}
		}
	}
	return "127.0.0.1"
}
