package util

import "time"

func NowTimeStr() string {
	return time.Now().Format("2006-01-02 15:04:05.000")
}

func FormatTime(time time.Time) string {
	return time.Format("2006-01-02 15:04:05.000")
}

func FormatTimeByStr(timeStr string, pattern string) string {
	t,_ := time.Parse(pattern, timeStr)
	return t.Format("2006-01-02 15:04:05.000")
}