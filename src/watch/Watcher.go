package watch

import (
	"../Log"
	"../model"
	"../kafka"
	"../fileConfig"
		"bufio"
	"os"
	)

type Watcher interface {
	Handler(config model.FileConfig, batchSize int)
}


type CreateFileWatcher struct {
	FilePath string
}

func (watcher CreateFileWatcher) Handler(config model.FileConfig) {
	Log.DebugWithFormat("创建文件: %s", watcher.FilePath)

	fileConfig.AddConfig(config)
	fileConfig.Print()
}



type WriteFileWatcher struct {
	FilePath string
}

func (watcher WriteFileWatcher) Handler(config model.FileConfig) {
	Log.DebugWithFormat("写入文件: %s", watcher.FilePath)
}



type DeleteFileWatcher struct {
	FilePath string
}

func (watcher DeleteFileWatcher) Handler(config model.FileConfig) {
	Log.DebugWithFormat("删除文件: %s", watcher.FilePath)

	fileConfig.RemoveConfig(config)
	fileConfig.Print()
}


type AppendFileWatcher struct {
	FilePath string
}

func (watcher AppendFileWatcher) Handler(config model.FileConfig, batchSize int) {
	Log.DebugWithFormat("追加文件: %s", watcher.FilePath)

	file, _ := os.Open(watcher.FilePath)
	fileScanner := bufio.NewScanner(file)
	lineCount := 1
	appendCount := 0
	for fileScanner.Scan(){
		if lineCount > config.LastScanLine {
			if appendCount < batchSize {
				kafka.AppendLog(kafka.NewKafkaLog(fileScanner.Text(), watcher.FilePath, lineCount))
			} else {
				break
			}

			appendCount++
		}
		lineCount++
	}

	defer file.Close()
	//文件最后会多一行
	lineCount = lineCount - 1

	if appendCount > 0 {
		Log.DebugWithFormat("追加文件:%s, %d->%d", watcher.FilePath, config.LastScanLine, lineCount)
	}

	kafka.UploadToKafka()
}